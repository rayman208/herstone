package com.company.tools;

import java.util.Dictionary;
import java.util.HashMap;

public class SettingList
{
    static private HashMap<String, Integer> Settings;

    static public void Init()
    {
        Settings = new HashMap<String, Integer>();

        Settings.put("AbilityBerserkConst",10);
        Settings.put("AbilityHealConst",10);
        Settings.put("AbilityDoubleDamageConst",10);

        Settings.put("DragonConst",10);
        Settings.put("DragonMinDmg",3);
        Settings.put("DragonMaxDmg",5);
        Settings.put("DragonMinHp",5);
        Settings.put("DragonMaxHp",10);

        Settings.put("ElfConst",6);
        Settings.put("ElfMinDmg",1);
        Settings.put("ElfMaxDmg",3);
        Settings.put("ElfMinHp",3);
        Settings.put("ElfMaxHp",5);

        Settings.put("KnightConst",8);
        Settings.put("KnightMinDmg",2);
        Settings.put("KnightMaxDmg",4);
        Settings.put("KnightMinHp",6);
        Settings.put("KnightMaxHp",8);
    }

    static public int Get(String key)
    {
        return Settings.get(key);
    }
}
