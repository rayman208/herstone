package com.company.tools;

import java.io.IOException;
import java.util.Scanner;

public class Console
{
    public static void Println(String string)
    {
        System.out.println(string);
    }

    public static void Print(String string)
    {
        System.out.print(string);
    }

    public static int InputInt(String message)
    {
        int x = 0;
        boolean inputResult = false;

        do
        {
            try
            {
                Scanner scanner = new Scanner(System.in);
                System.out.print(message);
                x = scanner.nextInt();
                inputResult=true;
            }
            catch (Exception e)
            {
                inputResult=false;
            }

        }while (inputResult==false);

        return x;
    }

    public static String InputString(String message)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print(message);
        return scanner.next();
    }

    public static void WaitEnter()
    {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    public static void Clear()
    {
        for (int i = 0; i < 50; i++)
        {
            System.out.println();
        }
    }
}
