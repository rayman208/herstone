package com.company.game;

public class AttackDefendPair
{
    public int IndexAttack;
    public int IndexDefend;

    public AttackDefendPair(int IndexAttack, int IndexDefend)
    {
        this.IndexAttack = IndexAttack;
        this.IndexDefend = IndexDefend;
    }
}
