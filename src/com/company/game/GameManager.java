package com.company.game;

import com.company.enities.*;
import com.company.tools.SettingList;
import com.company.tools.Console;

import java.awt.*;
import java.util.ArrayList;

public class GameManager
{
    private Player player1;
    private Player player2;
    private ArrayList<AttackDefendPair> attackDefendPairs;

    public GameManager()
    {
        SettingList.Init();
        attackDefendPairs = new ArrayList<>();
    }

    public void ClearScreen()
    {
        Console.Clear();
    }

    public void PrintMessage(String message)
    {
        Console.Println(message);
    }

    public void WaitEnter()
    {
        Console.Println("Press <Enter>");
        Console.WaitEnter();
    }

    public void BuyCardsForPlayer1()
    {
        Console.Println("Buy Card for Player 1");
        BuyCardsForPlayer(player1);
    }

    public void BuyCardsForPlayer2()
    {
        Console.Println("Buy Card for Player 2");
        BuyCardsForPlayer(player2);
    }

    public void PrintPlayer1Info()
    {
        Console.Println(player1.GetPlayerInfo());
        Console.Println(player1.GetCardsInfo());
    }

    public void PrintPlayer1ShortInfo()
    {
        Console.Println(player1.GetPlayerInfo());
    }

    public void PrintPlayer2Info()
    {
        Console.Println(player2.GetPlayerInfo());
        Console.Println(player2.GetCardsInfo());
    }

    public void PrintPlayer2ShortInfo()
    {
        Console.Println(player2.GetPlayerInfo());
    }

    public void InitPlayer1()
    {
        Console.Println("Init Player 1 parameters");
        player1 = InitPlayer();
    }

    public void InitPlayer2()
    {
        Console.Println("Init Player 2 parameters");
        player2 = InitPlayer();
    }

    public void SetActiveCardsForPlayer1Attack()
    {
        player1.ClearActiveCards();
        Console.Println("Set card for attack for player1");
        SetActiveCardsForPlayer(player1);
    }

    public void SetActiveCardsForPlayer2Defend()
    {
        player2.ClearActiveCards();
        Console.Println("Set card for defend for player2");
        SetActiveCardsForPlayer(player2);
    }

    public void PrintActiveCardsForPlayer1()
    {
        Console.Println("Player 1 active cards");
        Console.Println(player1.GetActiveCardsInfo());
    }

    public void PrintActiveCardsForPlayer2()
    {
        Console.Println("Player 2 active cards");
        Console.Println(player2.GetActiveCardsInfo());
    }

    public boolean Player1IsAlive()
    {
        return player1.IsAlive();
    }

    public boolean Player2IsAlive()
    {
        return player2.IsAlive();
    }

    public void Player1AttackPlayer2()
    {
        ChooseCardsToAttackAndDefend(player1, player2);
        ExchangeOfBlows(player1, player2);
    }

    private void ExchangeOfBlows(Player attackPlayer, Player defendPlayer)
    {
        ArrayList<Integer> indexesAttackCards = new ArrayList<>();
        for (int i = 0; i < attackDefendPairs.size(); i++)
        {
            indexesAttackCards.add(attackDefendPairs.get(i).IndexAttack);
        }

        ArrayList<Integer> unusableActiveCards = attackPlayer.GetUnusableActiveCards(indexesAttackCards);

        for (int i = 0; i < attackDefendPairs.size(); i++)
        {
            Card attackCard = attackPlayer.GetActiveCardByIndex(attackDefendPairs.get(i).IndexAttack);
            Card defendCard = defendPlayer.GetActiveCardByIndex(attackDefendPairs.get(i).IndexDefend);

            if(attackCard.CanAttack(defendCard)==true)
            {
                AttackValues values = attackCard.GetAttackValues();

                defendCard.TakeDamage(values.AttackPoints);

                attackCard.TakeHp(values.HpPoints);
            }

            if(defendCard.CanAttack(attackCard)==true)
            {
                AttackValues values = defendCard.GetAttackValues();

                attackCard.TakeDamage(values.AttackPoints);

                defendCard.TakeHp(values.HpPoints);
            }
        }

        for (int i = 0; i < unusableActiveCards.size(); i++)
        {
            Card attackCard = attackPlayer.GetActiveCardByIndex(unusableActiveCards.get(i));

            AttackValues values = attackCard.GetAttackValues();

            defendPlayer.TakeDamageInFace(values.AttackPoints);

            attackCard.TakeHp(values.HpPoints);
        }

        attackPlayer.RemoveDeadCards();
        defendPlayer.RemoveDeadCards();
    }

    private void ChooseCardsToAttackAndDefend(Player attackPlayer, Player defendPlayer)
    {
        int minCardsCount;

        if(attackPlayer.GetActiveCardsCount()<defendPlayer.GetActiveCardsCount())
        {
            minCardsCount = attackPlayer.GetActiveCardsCount();
        }
        else
        {
            minCardsCount = defendPlayer.GetActiveCardsCount();
        }

        attackDefendPairs.clear();

        for (int i = 0; i < minCardsCount; i++)
        {
            Console.Println("Choose "+(i+1)+" pair");

            int attackIndex = Console.InputInt("Input position attack card: ")-1;
            if(attackPlayer.GetActiveCardByIndex(attackIndex)==null)
            {
                Console.Println("Error. You haven't such card");
                i--;
                continue;
            }
            for (int j = 0; j < attackDefendPairs.size(); j++)
            {
                if(attackDefendPairs.get(i).IndexAttack==attackIndex)
                {
                    Console.Println("Error. Such cards already chosen");
                    i--;
                    continue;
                }
            }

            int defendIndex = Console.InputInt("Input position defend card: ")-1;
            if(defendPlayer.GetActiveCardByIndex(defendIndex)==null)
            {
                Console.Println("Error. You haven't such card");
                i--;
                continue;
            }
            for (int j = 0; j < attackDefendPairs.size(); j++)
            {
                if(attackDefendPairs.get(i).IndexDefend==defendIndex)
                {
                    Console.Println("Error. Such cards already chosen");
                    i--;
                    continue;
                }
            }

            attackDefendPairs.add(new AttackDefendPair(attackIndex, defendIndex));
        }
    }

    private Player InitPlayer()
    {
        String name = Console.InputString("Input name: ");
        int money = Console.InputInt("Input start money: ");
        int health = Console.InputInt("Input start health: ");

        return new Player(name,  money, health);
    }

    private void BuyCardsForPlayer(Player player)
    {
        while (true)
        {
            Console.Clear();
            Console.Println("1: " + DragonCard.GetInfo());
            Console.Println("2: " + KnightCard.GetInfo());
            Console.Println("3: " + ElfCard.GetInfo());

            Console.Println("");
            Console.Println("You have " + player.GetMoney() + " money");
            int numberOfCard = Console.InputInt("Choose number of card, which you want to buy(input 0 for exit): ");

            boolean buyResult=false;

            switch (numberOfCard)
            {
                case 1:
                    buyResult = player.BuyCard(new DragonCard());
                    break;
                case 2:
                    buyResult = player.BuyCard(new KnightCard());
                    break;
                case 3:
                    buyResult = player.BuyCard(new ElfCard());
                    break;
                case 0:
                    return;
            }

            if(buyResult==true)
            {
                Console.Println("Success buy");
            }
            else
            {
                Console.Println("You haven't enough money");
            }
            Console.WaitEnter();
        }
    }

    private void SetActiveCardsForPlayer(Player player)
    {
        int position;

        do
        {
            position = Console.InputInt("Input position card (input 0 for exit): ");

            if (position!=0)
            {
                boolean result = player.SetCardActive(position);

                if(result==true)
                {
                    Console.Println("Success choose");
                }
                else
                {
                    Console.Println("Choose error");
                }
            }

            Console.Print("Now you are choosing: ");
            Console.Println(player.GetActiveCardsNumbers());

        }while (position!=0);
    }
}
