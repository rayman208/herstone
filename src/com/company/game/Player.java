package com.company.game;

import com.company.enities.Card;

import java.util.ArrayList;

public class Player
{
    private ArrayList<Card> cards;
    private ArrayList<Integer> indexesActiveCards;
    private String name;
    private int money;
    private int health;

    public Player(String name, int money, int health)
    {
        this.name = name;
        this.money = money;
        this.health = health;

        cards = new ArrayList<>();
        indexesActiveCards = new ArrayList<>();
    }

    public boolean BuyCard(Card card)
    {
        if(money>=card.GetCost())
        {
            money-=card.GetCost();
            cards.add(card);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ClearActiveCards()
    {
        indexesActiveCards.clear();
    }

    public boolean SetCardActive(int position)
    {
        int index = position-1;
        if(index<0 || index>cards.size()-1)
        {
            return false;
        }

        if(indexesActiveCards.contains(index)==true)
        {
            return false;
        }

        indexesActiveCards.add(index);

        return true;
    }

    public String GetActiveCardsNumbers()
    {
        String output="";

        for (int i = 0; i < indexesActiveCards.size(); i++)
        {
            output+=(indexesActiveCards.get(i)+1)+" ";
        }

        return output;
    }

    public String GetActiveCardsInfo()
    {
        String output="";
        for (int i = 0; i < indexesActiveCards.size(); i++)
        {
            output+="Card"+(i+1)+":"+"\n";
            output+=cards.get(indexesActiveCards.get(i)).GetStringInfo()+"\n";
            output+="--------"+"\n";
        }
        return output;
    }

    public Card GetActiveCardByIndex(int index)
    {
        if(indexesActiveCards.contains(index)==false)
        {
            return null;
        }
        else
        {
            return cards.get(index);
        }
    }

    public ArrayList<Integer> GetUnusableActiveCards(ArrayList<Integer> usableActiveCards)
    {
        ArrayList<Integer> unusableActiveCards = new ArrayList<>();

        for (int i = 0; i < indexesActiveCards.size(); i++)
        {
            if(usableActiveCards.contains(indexesActiveCards.get(i))==false)
            {
                unusableActiveCards.add(indexesActiveCards.get(i));
            }
        }

        return unusableActiveCards;
    }

    public int GetActiveCardsCount()
    {
        return indexesActiveCards.size();
    }

    public String GetPlayerInfo()
    {
        String output="";
        output+="Player: "+name+"\n";
        output+="Money: "+money+"\n";
        output+="Health: "+health;
        return output;
    }

    public String GetCardsInfo()
    {
        String output="";
        for (int i = 0; i < cards.size(); i++)
        {
            output+="Card"+(i+1)+":"+"\n";
            output+=cards.get(i).GetStringInfo()+"\n";
            output+="--------"+"\n";
        }
        return output;
    }

    public void RemoveDeadCards()
    {
        for (int i = 0; i < cards.size(); i++)
        {
            if(cards.get(i).IsAlive()==false)
            {
                cards.remove(i);
                i=-1;
            }
        }
    }

    public void AddMoney(int money)
    {
        this.money+=money;
    }

    public int GetMoney()
    {
        return money;
    }

    public void TakeDamageInFace(int damage)
    {
        this.health-=damage;
    }

    public boolean IsAlive()
    {
        return health>0;
    }
}
