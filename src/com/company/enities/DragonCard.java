package com.company.enities;

import com.company.tools.Random;
import com.company.tools.SettingList;

public class DragonCard extends Card
{
    public DragonCard()
    {
        super("Dragon",
                Random.GetValue(SettingList.Get("DragonMinDmg"),SettingList.Get("DragonMaxDmg")),
                Random.GetValue(SettingList.Get("DragonMinHp"),SettingList.Get("DragonMaxHp")),
                TypeOfMove.Fly,
                SettingList.Get("DragonConst"));
    }

    public static String GetInfo()
    {
        return "Dragon, damage: "+SettingList.Get("DragonMinDmg")+"-"+SettingList.Get("DragonMaxDmg")+" health: "+SettingList.Get("DragonMinHp")+"-"+SettingList.Get("DragonMaxHp")+" cost:"+SettingList.Get("DragonConst");
    }
}
