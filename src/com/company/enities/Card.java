package com.company.enities;

import java.util.ArrayList;

public abstract class Card
{
    protected String name;
    protected int damage;
    protected int health;
    protected ArrayList<Ability> abilities;
    protected TypeOfMove typeOfMove;
    protected int cost;

    public Card(String name, int damage, int health, TypeOfMove typeOfMove, int cost)
    {
        this.name = name;
        this.damage = damage;
        this.health = health;
        this.typeOfMove = typeOfMove;
        this.cost = cost;

        abilities = new ArrayList<>();
    }

    public int GetCost()
    {
        return cost;
    }

    public boolean CanAttack(Card card)
    {
        if(this.typeOfMove == TypeOfMove.Walk && card.typeOfMove == TypeOfMove.Walk)
        {
            return true;
        }
        else if(this.typeOfMove == TypeOfMove.Fly && card.typeOfMove == TypeOfMove.Walk)
        {
            return true;
        }
        else if(this.typeOfMove == TypeOfMove.Fly && card.typeOfMove == TypeOfMove.Fly)
        {
            return true;
        }
        else if(this.typeOfMove == TypeOfMove.Walk && card.typeOfMove == TypeOfMove.Fly)
        {
            return false;
        }

        return false;
    }

    public AttackValues GetAttackValues()
    {
        int attackPoints = damage;
        int hpPoints = 0;

        for (int i = 0; i < abilities.size(); i++)
        {
            switch (abilities.get(i))
            {
                case Berserk:
                    attackPoints+=attackPoints*0.2;
                    break;
                case DoubleDamage:
                    attackPoints*=2;
                    break;
            }
        }

        for (int i = 0; i < abilities.size(); i++)
        {
            switch (abilities.get(i))
            {
                case Heal:
                    hpPoints+=damage*0.1;
                    break;
            }
        }

        return new AttackValues(attackPoints, hpPoints);
    }

    public void TakeDamage(int attackPoints)
    {
        health-=attackPoints;
    }

    public void TakeHp(int hpPoints)
    {
        health+=hpPoints;
    }

    public boolean IsAlive()
    {
        return health>0;
    }

    public void AddAbility(Ability ability)
    {
        abilities.add(ability);
    }

    public String GetStringInfo()
    {
        String output = "Name: " + name + " Damage: " + damage + " Health: " + health+"\nAbilities: ";

        if(abilities.size()==0)
        {
            output+="abilities list is empty";
        }
        else
        {
            for (int i = 0; i < abilities.size(); i++)
            {
                output += abilities.get(i)+" ";
            }
        }
        output+="\nType of move: "+typeOfMove;
        return output;
    }
}
