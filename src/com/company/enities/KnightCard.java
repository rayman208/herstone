package com.company.enities;

import com.company.tools.Random;
import com.company.tools.SettingList;

public class KnightCard extends Card
{
    public KnightCard()
    {
        super("Knight",
                Random.GetValue(SettingList.Get("KnightMinDmg"),SettingList.Get("KnightMaxDmg")),
                Random.GetValue(SettingList.Get("KnightMinHp"),SettingList.Get("KnightMaxHp")),
                TypeOfMove.Walk,
                SettingList.Get("KnightConst"));
    }

    public static String GetInfo()
    {
        return "Knight, damage: "+SettingList.Get("KnightMinDmg")+"-"+SettingList.Get("KnightMaxDmg")+" health: "+SettingList.Get("KnightMinHp")+"-"+SettingList.Get("KnightMaxHp")+" cost:"+SettingList.Get("KnightConst");
    }
}
