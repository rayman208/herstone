package com.company.enities;

import com.company.tools.Random;
import com.company.tools.SettingList;

public class ElfCard extends Card
{
    public ElfCard()
    {
        super("Elf",
                Random.GetValue(SettingList.Get("ElfMinDmg"),SettingList.Get("ElfMaxDmg")),
                Random.GetValue(SettingList.Get("ElfMinHp"),SettingList.Get("ElfMaxHp")),
                TypeOfMove.Walk,
                SettingList.Get("ElfConst"));
    }

    public static String GetInfo()
    {
        return "Elf, damage: "+SettingList.Get("ElfMinDmg")+"-"+SettingList.Get("ElfMaxDmg")+" health: "+SettingList.Get("ElfMinHp")+"-"+SettingList.Get("ElfMaxHp")+" cost:"+SettingList.Get("ElfConst");
    }
}
