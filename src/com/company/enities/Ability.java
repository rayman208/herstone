package com.company.enities;

public enum Ability
{
    Berserk,
    Heal,
    DoubleDamage
}
