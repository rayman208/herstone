package com.company;

import com.company.game.GameManager;

public class Main {

    public static void main(String[] args)
    {
        GameManager gameManager = new GameManager();

        gameManager.InitPlayer1();
        gameManager.BuyCardsForPlayer1();
        gameManager.ClearScreen();
        gameManager.PrintPlayer1Info();
        gameManager.WaitEnter();

        gameManager.ClearScreen();

        gameManager.InitPlayer2();
        gameManager.BuyCardsForPlayer2();
        gameManager.ClearScreen();
        gameManager.PrintPlayer2Info();
        gameManager.WaitEnter();

        while (gameManager.Player1IsAlive() && gameManager.Player2IsAlive())
        {
            gameManager.ClearScreen();

            gameManager.PrintPlayer1Info();
            gameManager.PrintMessage("Player 1 is attacking now");
            gameManager.SetActiveCardsForPlayer1Attack();

            gameManager.ClearScreen();

            gameManager.PrintPlayer2Info();
            gameManager.PrintMessage("Player 2 is defending now");
            gameManager.PrintActiveCardsForPlayer1();
            gameManager.SetActiveCardsForPlayer2Defend();

            gameManager.ClearScreen();

            gameManager.PrintPlayer1ShortInfo();
            gameManager.PrintPlayer2ShortInfo();
            gameManager.PrintMessage("Player 1 IS ATTACKING Player 2");
            gameManager.PrintActiveCardsForPlayer1();
            gameManager.PrintActiveCardsForPlayer2();

            gameManager.Player1AttackPlayer2();

            gameManager.ClearScreen();
            gameManager.PrintPlayer1Info();
            gameManager.PrintPlayer2Info();
            gameManager.PrintMessage("Player 1 finish, Player 2 start");

            gameManager.WaitEnter();


            //todo player 2 turn
        }

        gameManager.ClearScreen();

        if(gameManager.Player1IsAlive()==false && gameManager.Player2IsAlive()==true)
        {
            gameManager.PrintMessage("Player 2 won");
        }
        else if(gameManager.Player1IsAlive()==true && gameManager.Player2IsAlive()==false)
        {
            gameManager.PrintMessage("Player 1 won");
        }
        else if(gameManager.Player1IsAlive()==false && gameManager.Player2IsAlive()==false)
        {
            gameManager.PrintMessage("Draw");
        }
        gameManager.WaitEnter();
    }
}
